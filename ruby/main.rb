require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    # tcp.puts createRace(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      gameTick = message['gameTick']
      case msgType
        when 'carPositions'
          dis = msgData[0]['piecePosition']['inPieceDistance']
          @current_data = msgData[0]['piecePosition']
          @current_piece = @pieceInfo[msgData[0]['piecePosition']['pieceIndex']]
          makeSwitch(tcp) if wanaSwitch?
          use_turbo if can_use_turbo?
          tcp.puts throttle_message(get_speed)
        else
          case msgType
            when 'join'
              puts 'Joined'
            when 'gameInit'
              @speed = 0
              @available_turbo = false
              @switchDirection = 'Right'
              @pieceInfo = msgData['race']['track']['pieces']
              getTotalLength
              getTurnPieces
            when 'gameStart'
              puts 'Race started'
            when 'turboAvailable'
              @available_turbo = true
            when 'crash'
              puts 'Someone crashed'
              puts msgData
            when 'lapFinished'
              puts 'lapFinished'
              puts msgData['lapTime']
            when 'gameEnd'
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
          end
          puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def get_speed
   p @current_data["pieceIndex"]
   nxt_piece = @current_data["pieceIndex"]+2
   nxt_piece = 0 if nxt_piece >=40
   vdec = (@speed > 0.65) ? 0.02 : 0.018 
   @speed -= vdec and return @speed if @pieceInfo[nxt_piece].include?('angle') && @speed >= 0.61
   if @current_piece.include?('angle')
    vinc = (@speed>0.5) ? -0.001: -0.0004
   else
    vinc = 0.016
   end
   @speed += vinc if @speed <= 0.9
   # @speed = 0.65
   p 'speed'
   p @speed
   return @speed
  end

  def getTurnPieces
    @turnIndies = []
    i =0
    @pieceInfo.each do |piece|
      @turnIndies << i if piece.include?('angle')
      i+=1
    end
  end

  def getTotalLength
    length = 0
    l = 0
    @pieceInfo.each do |piece|
      if piece.include?('angle')
        r = piece['radius']
        deg = piece['angle']
        l = (deg / 360) * (2 * 3.14 * r)
      else
        l = piece['length']
      end
      length += l
    end
  end

  def wanaSwitch?
    nxt_piece = @current_data["pieceIndex"]+1
    @current_piece.include?('switch') && @pieceInfo[nxt_piece].include?('angle')
  end

  def makeSwitch (tcp)
    @switchDirection = toggleDirection
    tcp.puts make_msg("switchLane", @switchDirection )
  end

  def toggleDirection
    nxt_piece = @current_data["pieceIndex"]+1
    (@pieceInfo[nxt_piece]['angle'] > 0) ? 'Right' : 'Left'
  end

  def use_turbo
    p 'on turbo'
    make_msg('turbo', 'on turbo')
    @available_turbo = false
  end

  def can_use_turbo?
    nxt_piece = @current_data["pieceIndex"]+3
    nxt_piece -= 39 if nxt_piece >=40
    @available_turbo && !@pieceInfo[nxt_piece].include?('angle') && !@current_piece.include?('angle')
  end

  def createRace(bot_name, bot_key)
    make_msg("createRace", {
      :botId => {
        :name => bot_name,
        :key => bot_key
      },
      :trackName => "usa",
      :carCount => 1
      })
  end
end

NoobBot.new(server_host, server_port, bot_name, bot_key)
